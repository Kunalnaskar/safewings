import 'package:flutter/material.dart';

class Payment {
  final int id;
  final IconData icon;
  final String title;
  final String subTitle;
  final String description;

  Payment({this.id, this.icon, this.title, this.subTitle, this.description});

  static List<Payment> getPayments() {
    return <Payment>[
      Payment(id: 1, icon: Icons.payment, title: "Paytm", subTitle: "Cashback upto 500 on min 300 order value", description: "Some info"),
      Payment(id: 2, icon: Icons.payment, title: "Amazon Pay", subTitle: "Cashback upto 500 on min 300 order value", description: "Some info"),
    ];
  }
}
