Calculator - Calculation
-------------------------------

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Application name
      title: 'Flutter Hello World',
      // Application theme data, you can set the colors for the application as
      // you want
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // A widget which will be started on application startup
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final t1 = TextEditingController();
  //  final t2 = TextEditingController();

  var num1 = 0, num2 = -1, mul = 0;

  void doMul() {
    setState(() {
      num1 = int.parse(t1.text);
      num2 = int.parse('23');
      mul = num1 * num2 + 2;
      /*   if (num2 == 1) {
        mul = num1 * num2;
      } else {
        mul = num1 * num2 - 1;
      } */
    });
  }

  void doClear() {
    setState(() {
      mul = 0;
      t1.text = "";
      //  t2.text = "";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Calculation"),
      ),
      body: Container(
        padding: EdgeInsets.all(40.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Total Amount= ${mul} Rs",
              style: TextStyle(
                color: Colors.red,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            // Use the method to get distance insted of this textfield.
            TextField(
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                hintText: 'Enter number',
              ),
              controller: t1,
            ),
            /*   TextField(
              decoration: InputDecoration(
                hintText: 'Enter number',
              ),
              controller:t2,
            ), */
            SizedBox(height: 20),
            MaterialButton(
              color: Colors.blue,
              onPressed: doMul,
              child: Text(
                "*",
                style: TextStyle(
                  fontSize: 30,
                  color: Colors.red,
                ),
              ),
            ),
            SizedBox(height: 20),
            MaterialButton(
              color: Colors.grey,
              onPressed: doClear,
              child: Text(
                'Clear',
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
_____________________________________________________________________________________