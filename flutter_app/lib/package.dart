import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(),
          body: SafeArea(
              child: Center(
            child: CheckboxWidget(),
          ))),
    );
  }
}

class CheckboxWidget extends StatefulWidget {
  @override
  CheckboxWidgetState createState() => new CheckboxWidgetState();
}

class CheckboxWidgetState extends State {
var tmpArray = [];
  Map<String, bool> values = {
    'DOCUMENTS | BOOK': false,
    'CLOTHES | ACCESSORIES': false,
    'FOOD | FLOWERS': false,
    'HOUSEHOLD ITEMS': false,
    'SPORTS & OTHER EQUIPMETS': false,
  };

//   getCheckboxItems() {
//     values.forEach((key, value) {
//       if (value == true) {
//         tmpArray.add(key);
//       }
//     });
//     return tmpArray;
//     //  tmpArray.clear();
//   }

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
     Container(  
          color: Colors.red,
          child: ListTile(
            title: const Text('Save your Time!', style: TextStyle(color: Color(0xffffffff), fontWeight: FontWeight.bold, fontSize: 15)),
            subtitle: const Text('Pickup or drop forgotten chargers,documents,food,laundry,items for repairs etc', style: TextStyle(color: Color(0xffffffff), fontSize: 11)),
          )),
             Expanded(
            child: Center(
            //   child: Text(tmpArray[values],
            //       style: TextStyle(
            //         fontSize: 20.0,
            //         fontWeight: FontWeight.bold,
            //       )),
            ),
          ),
          Expanded(
        child: ListView(
          children: values.keys.map((String key) {
            return new CheckboxListTile(
              title: new Text(key),
              value: values[key],
              activeColor: Colors.red,
              checkColor: Colors.white,
              onChanged: (bool value) {
                setState(() {
                  values[key] = value;
                   values.forEach((key, value) {
      if (value == true) {
        tmpArray.add(key);
      }
      return tmpArray;
    });
                });
              },
            );
          }).toList(),
        )
      ),   
       Padding(
        padding: EdgeInsets.all(15),
        child: new ButtonBar(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            RaisedButton(
              child: new Text('Cancel'),
              color: Colors.grey,
              padding: EdgeInsets.fromLTRB(30, 15, 30, 15),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),
              onPressed: () {},
            ),
            SizedBox(width: 5),
            FlatButton(
              child: Text('Proceed'),
              color: Colors.red,
              padding: EdgeInsets.fromLTRB(30, 15, 30, 15),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),
              onPressed: () {},
            ),
          ],
        ),
      ),
    ]);
  }
}