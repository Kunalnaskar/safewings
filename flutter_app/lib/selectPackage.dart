import 'package:flutter/material.dart';
import 'package:hello_world/payment.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          body: SafeArea(
              child: Center(
        child: CheckboxWidget(),
      ))),
    );
  }
}

class CheckboxWidget extends StatefulWidget {
  @override
  CheckboxWidgetState createState() => new CheckboxWidgetState();
}

class CheckboxWidgetState extends State {
  var tmpArray = [];
  Map<String, bool> values = {
    'DOCUMENTS | BOOK': false,
    'CLOTHES | ACCESSORIES': false,
    'FOOD | FLOWERS': false,
    'HOUSEHOLD ITEMS': false,
    'SPORTS & OTHER EQUIPMETS': false,
  };

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Container(
          color: Colors.red,
          padding: EdgeInsets.fromLTRB(10, 50, 10, 50),
          child: ListTile(
            title: const Text('Save your Time!', style: TextStyle(color: Color(0xffffffff), fontWeight: FontWeight.bold, fontSize: 15)),
            subtitle: const Text('Pickup or drop forgotten chargers,documents,food,laundry,items for repairs etc', style: TextStyle(color: Color(0xffffffff), fontSize: 11)),
          )),
      Expanded(child: ListView.builder(
          itemBuilder: (BuildContext context, int index) {
            String key = values.keys.elementAt(index);
            if (values[key] == true) {
              return new Column(
                children: <Widget>[
                  new ListTile(
                    title: new Text("$key"),
                  ),
                ],
              );
            }
          },
        )),
      Expanded(
          child: ListView(
        children: values.keys.map((String key) {
          return new CheckboxListTile(
            title: new Text(key),
            value: values[key],
            activeColor: Colors.red,
            checkColor: Colors.white,
            onChanged: (bool value) {
              setState(() {
                values[key] = value;
              });
            },
          );
        }).toList(),
      )),
      Padding(
        padding: EdgeInsets.all(15),
        child: new ButtonBar(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            RaisedButton(
              child: new Text('Cancel'),
              color: Colors.grey,
              padding: EdgeInsets.fromLTRB(30, 15, 30, 15),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),
              onPressed: () {},
            ),
            SizedBox(width: 5),
            FlatButton(
              child: Text('Proceed'),
              color: Colors.red,
              padding: EdgeInsets.fromLTRB(30, 15, 30, 15),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => MyHomePage()));
              },
            ),
          ],
        ),
      ),
    ]);
  }
}
