import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget i
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // Application name
      title: 'Flutter Hello World',
      // Application theme data, you can set the colors for the application as
      // you want
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // A widget which will be started on application startup
      home: MyHomePage(
        storage: MyStorage(),
      ),
    );
  }
}

class MyStorage {
  // get directory path
  Future<String> get localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  //get file
  Future<File> get localFile async {
    final path = await localPath;
    return File('$path/man.txt');
  }

  //Read file contents
  Future<String> readContent() async {
    try {
      final file = await localFile;
      String contents = await file.readAsString();
      return contents;
    } catch (e) {
      return e; // Default null
    }
  }

  // Write file contents
  Future<File> writeContent(String content) async {
    final file = await localFile;
    return file.writeAsString('$content');
  }
}

class MyHomePage extends StatefulWidget {
  final MyStorage storage;

  MyHomePage({Key key, @required this.storage}) : super(key: key);
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // Create Controllers to get text
  final controller = TextEditingController();
  final route = TextEditingController();
  final numbers = TextEditingController();
  String text = '';
  String radioButtonItem = "HOME";
  int id = 1;


  Future<File> saveToFile() async {
    setState(() {
      text = controller.text;
    });
    return widget.storage.writeContent(text);
  }

  @override
  void initState() {
    super.initState();
    widget.storage.readContent().then((String value) {
      setState(() {
        text = value; // load text default
      });
    });
  }

  @override
  void readFile() {
    widget.storage.readContent().then((String value) {
      setState(() {
        text = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Read and Write'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(
              hintText: 'Enter Location',
              labelText: 'Flat, Flore, Building Name',
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(color: Colors.black),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(color: Colors.teal),
              ),
              prefixIcon: Icon(Icons.home),
            ),
            controller: controller,
          ),
          SizedBox(height: 20),
          // Text('$text'),
          // SizedBox(height: 20),
          TextFormField(
            decoration: InputDecoration(
              hintText: 'Select Route',
              labelText: 'How to Reach (optional)',
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(color: Colors.black),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(color: Colors.teal),
              ),
              prefixIcon: Icon(Icons.near_me),
            ),
            controller: route,
          ),
          SizedBox(height: 20),
          TextFormField(
            decoration: InputDecoration(
              hintText: '8050201025',
              labelText: 'Contact Details (optional)',
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(color: Colors.black),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                borderSide: BorderSide(color: Colors.teal),
              ),
              prefixIcon: Icon(Icons.phone_in_talk),
            ),
            controller:numbers,
          ),
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(0.0),
                child: RaisedButton(
                  onPressed: () {
                    saveToFile();
                  },
                  child: Text('Add Delivary Address'),
                  color: Colors.red,
                ),
              ),
              SizedBox(width: 10),
            ],
          ),
          Text('$text - $radioButtonItem'),
          Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Radio(
              value: 1,
              groupValue: id,
              onChanged: (val) {
                setState(() {
                  radioButtonItem = 'HOME';
                  id = 1;
                });
              },
            ),
            Text(
              'Home',
              style: new TextStyle(fontSize: 17.0),
            ),
 
            Radio(
              value: 2,
              groupValue: id,
              onChanged: (val) {
                setState(() {
                  radioButtonItem = 'OFFICE';
                  id = 2;
                });
              },
            ),
            Text(
              'Office',
              style: new TextStyle(
                fontSize: 17.0,
              ),
            ),
            
            Radio(
              value: 3,
              groupValue: id,
              onChanged: (val) {
                setState(() {
                  radioButtonItem = 'OTHERS';
                  id = 3;
                });
              },
            ),
            Text(
              'Others',
              style: new TextStyle(fontSize: 17.0),
            ),
          ],
        ),
       

         
          SizedBox(height: 20),
          RaisedButton(
            onPressed: () {
              readFile();
            },
            child: Text("Read from Address"),
            color: Colors.red,
          ),
        ],
      ),
    );
  }
}